# eTrips HTML template #

Created | 20. Jun. 2015
--------|-------
Author  | [Anze Klancar](https://twitter.com/iKlancar)

### What is this repository for? ###
This is source repo for building eTrips HTML template.

### What's builg upon? ###
* [Bootstrap](http://getbootstrap.com/) framework
* [Handlebars](http://handlebarsjs.com/) templating system
* [LESS](http://lesscss.org/) compiling

It uses (with Grunt dependencies) some great tools that enables you to:

* [watch](https://github.com/gruntjs/grunt-contrib-watch) files - Run predefined tasks whenever watched file patterns are added, changed or deleted.
* [live reload](https://github.com/gruntjs/grunt-contrib-livereload) browser window to see changes in real time
* css minify to minify and clean stylesheets in production
* html-clean to minify and clean html or php files in production
* ftp-deploy to copy distibution files to desired location

### How do I set up environment? ###
You will need to install the following - in that specific order:

* [Node.js](http://nodejs.org/) or [npm](https://npmjs.org/), package manager (same thing, but binary installation is on first url)
* [Grunt](http://gruntjs.com/getting-started) JavaScript Task Runner

It's recommended to use some Git / Mercurial client like [SourceTree](https://www.sourcetreeapp.com/), to keep up with repo changes. Also, some PHP development environment is needed to display local distribution files.
[MAMP](https://www.mamp.info/en/downloads/) or [XAMPP](https://www.apachefriends.org/download.html) is recommended.


### How do I build up the project? ###

	$ cd to-your-local-repo
	$ npm install 
	$ grunt
	
You will be provided with additional info from Terminal response.

<hr>
© THIS REPOSITORY IS UNDER STRICT COPY RIGHT AND MAY CONTAIN CONFIDENTIAL AND / OR PRIVILEGED INFORMATION. ANY UNAUTHORIZED MODIFICATION, COPYING, DISCLOSURE OR DISTRIBUTION OF THAT REPOSITORY OR IT'S CONTENT IS STRICTLY FORBIDDEN.