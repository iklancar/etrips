'use strict';

module.exports = function(grunt) {

	const sass = require('node-sass');

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        watch: {
            options: {
                interval: 1500
            },
            sass: {
                files: ['src/scss/*.scss', 'src/scss/*/**.scss'],
                tasks: ['sass:development'],
                options: {
                    livereload: true
                }
            },
            templates: {
                files: ['src/templates/**/*.hbs'],
                tasks: ['compile-handlebars'],
                options: {
                    livereload: true
                }
            },
            partials: {
                files: ['src/partials/**/*.hbs'],
                tasks: ['compile-handlebars'],
                options: {
                    livereload: true
                }
            }
        },
        clean: ["dist"],
		sass: {
			development: {
				options: {
					implementation: sass,
					sourceMap: true,
				},
				files: {
					// 'dist/css/page_name.css': 'src/scss/page_name.scss',
					'dist/css/pridruzi_se.css': 'src/scss/pridruzi_se.scss',
					'dist/css/all.css': 'src/scss/all.scss'
				}
			},
			production: {
				options: {
					implementation: sass,
					sourceMap: false,
					outputStyle: 'compressed'
				},
				files: {
					// 'dist/css/page_name.css': 'src/scss/page_name.scss',
					'dist/css/pridruzi_se.css': 'src/scss/pridruzi_se.scss',
					'dist/css/all.css': 'src/scss/all.scss'
				}
			}
		},
        'compile-handlebars': {
            default: {
                template: 'src/templates/**/*.hbs',
                templateData: 'src/data/**/*.json',
                output: 'dist/**/*.html',
                partials: 'src/partials/**/*.hbs',
                globals: [
                    {
                        set: {
                            'root' : ''
                        }
                    }
                ]
            }
        },
        copy: {
            'img': {
                files: [
                    {
                        expand: true,
                        cwd: 'src/img',
                        src: '**/*',
                        dest: 'dist/img',
                        filter: 'isFile',
                        // flatten: true,
                    }
                ]
            },
            'js': {
                files: [
                    {
                        expand: true,
                        cwd: 'src/js',
                        src: '**/*',
                        dest: 'dist/js',
                        filter: 'isFile',
                    }
                ]
            },
            'bootstrap': {
                files: [
                    {
                        expand: true,
                        src: ['node_modules/bootstrap/fonts/*'],
                        dest: 'dist/fonts',
                        flatten: true,
                        filter: 'isFile'
                    },
                    {
                        expand: true,
                        src: ['node_modules/bootstrap/dist/**/*.js'],
                        dest: 'dist/js',
                        flatten: true,
                        filter: 'isFile'
                    }
                ]
			},
			'webfonts': {
                files: [
                    {
                        expand: true,
                        src: ['src/webfonts/*'],
                        dest: 'dist/webfonts',
                        flatten: true,
                        filter: 'isFile'
                    }
                ]
            }
            // 'htaccess': {
            //     files: [
            //         {
            //             expand: true,
            //             src: 'src/.htaccess',
            //             dest: 'dist/',
            //             filter: 'isFile',
            //             flatten: true
            //         }
            //     ]
            // }
        },
        htmlclean: {
            deploy: {
                expand: true,
                cwd: 'dist',
                src: '**/*.html',
                dest: 'dist'
            }
        },
        // imagemin: {
        //     dynamic: {
        //         files: [{
        //             expand: true,
        //             cwd: 'dist/img',
        //             src: ['**/*.{png,jpg,gif}'],
        //             dest: 'dist/'
        //         }]
        //     }
        // },
        // concat: {
        //     options: {
        //         stripBanners: true,
        //         banner: '/* <%= pkg.name %> -> <%= pkg.accName %> - v<%= pkg.version %> - ' +
        //                 '<%= grunt.template.today("yyyy-mm-dd HH:MM:ss") %>\n' +
        //                 '<%= pkg.homepage ? "* " + pkg.homepage + "\\n" : "" %>' +
        //                 ' * Copyright (c) <%= grunt.template.today("yyyy") %> <%= pkg.author %>;' +
        //                 ' Licensed <%= _.pluck(pkg.licenses, "type").join(", ") %> */\n' +
        //                 '\n',
        //         },
        //         dist: {
        //             src: ['dist/test.html'],
        //             dest: 'dist/test.html',
        //     },
        // },
        'ftp-deploy': {
            'staging': {
                auth: {
                    host: 'ftp.prezen.ca',
                    port: 21,
                    authKey: 'privateKey'
                },
                src: 'dist',
                dest: '/subdomains/etrips/pridruzise/',
                exclusions: ['dist/.DS_Store', 'dist/Thumbs.db', 'dist/tmp'],
                server_sep: '/'
            }
        }
    });

    // Load Grunt plugins
    require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

    grunt.registerTask('make', ['compile-handlebars']);
    grunt.registerTask('dev', ['clean', 'sass:development', 'compile-handlebars', 'copy']);
    grunt.registerTask('production', ['clean', 'sass:production', 'compile-handlebars', 'copy', 'ftp-deploy']);

    grunt.task.registerTask('default', 'default', function(id, debug) {
        grunt.log.subhead('grunt dev');
        grunt.log.writeln('Builds all files for local development. Availible at "www.xxx-xxx.dev".');
        grunt.log.subhead('grunt production');
        grunt.log.writeln('Builds all files and ftp to live server. Availible at "www.xxx-xxx.si".');
    });

};